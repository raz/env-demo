# env-demo

`menu.sh` requires `jq`

http://wpaz1.kiteflyingdutchman.com/wp-admin

plugins:

- Envato Elements
- Elementor
- Elementor Pro

theme:

- Hello

Elements > Template Kits > pick a template kit > open Home page \
save image as "Cover - Template Name" \
import image in Media Library

Create dummy page "=Template Name" \
this will serve as parent page in page list, but never be shown

Elements > Template Kits > pick a template kit \
Open each page template \
Create page from template, use "Template Name - Pagename" as name for new page

Bulk Edit the just created pages \
Parent = "=Template Name" dummy page \
Template = Elementor Full Width \
Status = Published

Is there a Popup page? \
Edit page with elementor \
Section: Content Width = Boxed \
Display condition = Singular > Any Child Of > "Template Name" dummy

Edit the main `@Home page` \
Title = "Template Name" \
Image = pick "Cover - Template Name" image \
  Link to "Template Name - Home"

Create new menu \
menu.sh 

Header \
Copy header from existing \
Change title to "Template Name" \
Change menu to "Template Name" menu \
Display condition = Singular > Any Child Of > "Template Name" dummy

