#!/usr/bin/env bash

menu () {
  site=$1
  echo "Creating menu - $site"

  # Get id of parent page
  parentid=`wp post list --allow-root --post_type=page --fields=ID --pagename="$site" --format=ids`
  # Get list of subpages
  pages=`wp post list --allow-root --post_type=page --fields=ID,post_title --post_parent=$parentid --format=json`
  nrpages=`echo $pages | jq 'length'`

  # Create menu
  menuid=`wp menu create "$site" --allow-root --porcelain`
  # Create menu item Home, Info, About, and Contact
  homeid=`wp menu item add-custom "$site" "Home" "#" --allow-root --porcelain`
  infoid=`wp menu item add-custom "$site" "Info" "#" --allow-root --porcelain`
  aboutid=`wp menu item add-custom "$site" "About" "#" --allow-root --porcelain`
  contactid=`wp menu item add-custom "$site" "Contact" "#" --allow-root --porcelain`

  for (( i=0; i < $nrpages; i++ ))
  do
    pageid=`echo $pages | jq ".[$i] | .ID"`
    pagetitle=`echo $pages | jq ".[$i] | .post_title"`
    menutitle=${pagetitle:${#site}+3}
    menutitle=${menutitle::-1}
    case "$menutitle" in 
      *Home*)
        wp menu item add-post "$site" $pageid --title="$menutitle" --parent-id=$homeid --allow-root
        ;;
      *About*)
        wp menu item add-post "$site" $pageid --title="$menutitle" --parent-id=$aboutid --allow-root
        ;;
      *Contact*)
        wp menu item add-post "$site" $pageid --title="$menutitle" --parent-id=$contactid --allow-root
        ;;
      *)
        wp menu item add-post "$site" $pageid --title="$menutitle" --parent-id=$infoid --allow-root
        ;;
    esac
  done
}

menu "Template name"
